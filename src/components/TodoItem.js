// 1. 先創一個 components 資料夾
// 2. 創一個 function ，這次是要把渲染網頁的部分移出來，所以取名 TodoItm
// 3. 要給其他檔案用，所以前面要加上 export default
// 4. 要拿 parent(App.js)的資料，所以帶入(props)
// 5. 把props那邊的資料存到變數 ex. const item = props.item

import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link, useHistory } from 'react-router-dom';

export default function TodoItem(props) {
  const { item, checkTodo, deleteTodo, changeTodo, editTodo } = props; // const item = props.item;
  const btnText = item.edit ? '編輯' : '結束編輯';

  // 用 useHistory 傳字串
  const history = useHistory();
  const handleClick = () => history.push(`/itemDetail/${ item.key }/${item.thing}`);

  return (
    <Router>
      <li key={item.key} id={item.key}>
      <input
        type="checkbox"
        value="checkbox" 
        onClick={checkTodo}
      />
      <span className={item.check ? "checked" : null}>{item.thing}</span>
      <input
        type="button"
        value="刪除"
        onClick={deleteTodo}
      />
      {
        item.edit ? null : (
        <input
          type="text"
          onChange={changeTodo}
        />
        )
      }
      <button onClick={editTodo}>
        {btnText}
      </button>
      {/* 不要用 Link 的方式傳參數，而是用 event 的方式 */}
      <button onClick={ handleClick }>
        詳細編輯
      </button>
      </li>
    </Router>
  );
}