import React, { Component } from 'react';
import App from '../App';
import { useRouteMatch, useLocation, useParams } from 'react-router-dom';

function ItemDetail(props){
  // 拿網址後面的 params 要用 useRouteMatch
  const match = useRouteMatch('/itemDetail/:id/:thing');

  return(
    <div>
      <h1>Todo詳細頁面</h1>
      <p>待辦事項：{ match.params.thing }</p>
    </div>
  )
}

export default ItemDetail;