import React from 'react'
import './App.css';
import TodoItem from './components/TodoItem';
import ItemDetail from './components/ItemDetail';
import { BrowserRouter as Router, Link, Switch, Route} from 'react-router-dom';

// 用來判斷「全部」「已完成」
const Mode = {
  All: 'all',
  Done: 'done',
  Undone: 'undone'
};

class TodoList extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      todoStr : [],
      nowInput : '',
      mode: Mode.All,
      moneyForSister: 40,
      moneyForBrother: 60
    };
  }

  onInputChange = (ev) => {
    this.setState({
      nowInput : ev.target.value
    });
  }

  addTodo = (ev) => {
    this.setState({
      todoStr : [
        ...this.state.todoStr, {
          thing: this.state.nowInput,
          key: Date.now(),
          edit: true,
          check: false,
          show: true
        }
      ]
    });
  }

  deleteTodo = (ev, idx) =>{
    this.setState({
                                        // 索引跟點擊的idx一樣就不要
      todoStr: this.state.todoStr.filter((_, index) => index !== idx)
    });
  }

  editTodo = (ev, idx) =>{
    const newTodoStr = this.state.todoStr.slice();
    newTodoStr[idx].edit = !newTodoStr[idx].edit;
    this.setState({
      todoStr: newTodoStr
    })
  }

  changeTodo = (ev, idx) =>{
    let newTodoStr = this.state.todoStr.slice();
    newTodoStr[idx].thing = ev.target.value;
    this.setState({
      todoStr: newTodoStr
    })
  }

  checkTodo = (ev, idx) =>{
    let newTodoStr = this.state.todoStr.slice();
    newTodoStr[idx].check = !newTodoStr[idx].check
    this.setState({
      todoStr: newTodoStr
    })
  }

  changeMode = (mode) => {
    this.setState({ mode });
  }

  allocateMoney = (target, amount) => {
    if(target === 'Brother'){
      this.setState({
        moneyForBrother: amount
      })
    }else{
      this.setState({
        moneyForSister: amount
      })
    }
  }

  render() {
    return (
      <div>
      <input type="text" id="text" value={this.state.nowInput} onChange={(ev) => this.onInputChange(ev)}/>
      <input type="button" value="新增" onClick={(ev) => this.addTodo(ev)} />
      <ul>
        {
          this.state.todoStr.filter((item) => {
            switch (this.state.mode) {
              case Mode.Done:
                return item.check;

              case Mode.Undone:
                return !item.check;

              case Mode.All:
              default:
                return true;
            }
          }).map((item, idx) => {
            
            return (
              <TodoItem
                item={item}
                key={item.key}
                checkTodo={(ev) => this.checkTodo(ev, idx)}
                deleteTodo={(ev) => this.deleteTodo(ev, idx)}
                changeTodo={(ev) => this.changeTodo(ev, idx)}
                editTodo={(ev) => this.editTodo(ev, idx)}
              />
            )
          
          })
        }
      </ul>
      <button value="全部" onClick={() => this.changeMode(Mode.All)}>全部</button>
      <button value="已完成" onClick={() => this.changeMode(Mode.Done)}>已完成</button>
      </div>
    )
  }
}

function App(props) {
  const name = props.name;
  return (
    <Router>
      <div>
      <h1>Todo List</h1>
      <Switch>
        <TodoList path="/" exact component={ TodoList } />
        <ItemDetail path="/itemDetail/:id"  component={ ItemDetail } />
      </Switch>
      </div>
    </Router>
  );
}

export default App;
